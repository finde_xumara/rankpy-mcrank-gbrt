Cython>=0.21.1
numpy>=1.8.1
scipy>=0.14.0
scikit-learn>=0.14.1
