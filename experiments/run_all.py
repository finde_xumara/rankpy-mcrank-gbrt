# -*- coding: utf-8 -*-

import numpy as np
import os
import logging

from rankpy.queries import Queries
from rankpy.models import LambdaMART, LambdaRandomForest
from rankpy.models import McRank, OrdinalMcRank, RegressionMcRank
from rankpy.metrics import NormalizedDiscountedCumulativeGain

from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.base import BaseEstimator
from sklearn.ensemble.gradient_boosting import LeastSquaresError

rf_trees = 1000
rf_trees_igbrt = 300
gbrt_estimators = 300
verbosity = 1




# Turn on logging.
logging.basicConfig(format='%(asctime)s : %(message)s', level=logging.INFO)

class init_multiclass(BaseEstimator):
    def __init__(self, est):
        self.est = est
    
    def predict(self, X):
        y = self.est.predict_proba(X)
        return y
    
    def fit(self, X, y):
        y = y.astype(np.int32)
        self.est.fit(X, y)
        return self

class init_singleclass(BaseEstimator):
    def __init__(self, est):
        self.est = est
    
    def predict(self, X):
        y = self.est.predict_proba(X)
        y = np.amax(y, axis = 1)[:,np.newaxis]
        return y
    
    def fit(self, X, y):
        y = y.astype(np.int32)
        self.est.fit(X, y)
        return self



def load_data(fold_n):
    cur_dir = os.path.dirname(os.path.realpath(__file__))

    try:
        train_queries = Queries.load(cur_dir + '/data/fold'+str(fold_n)+'_train')
        valid_queries = Queries.load(cur_dir + '/data/fold'+str(fold_n)+'_vali')
        test_queries = Queries.load(cur_dir + '/data/fold'+str(fold_n)+'_test')
    except IOError as e:
        # Load the query datasets.
        train_queries = Queries.load_from_text(cur_dir + '/data/MQ2008/Fold'+str(fold_n)+'/train.txt')
        valid_queries = Queries.load_from_text(cur_dir + '/data/MQ2008/Fold'+str(fold_n)+'/vali.txt')
        test_queries = Queries.load_from_text(cur_dir + '/data/MQ2008/Fold'+str(fold_n)+'/test.txt')

        logging.info('================================================================================')

        # Save them to binary format ...
        train_queries.save(cur_dir + '/data/fold'+str(fold_n)+'_train')
        valid_queries.save(cur_dir + '/data/fold'+str(fold_n)+'_vali')
        test_queries.save(cur_dir + '/data/fold'+str(fold_n)+'_test')

        # ... because loading them will be then faster.
        train_queries = Queries.load(cur_dir + '/data/fold'+str(fold_n)+'_train')
        valid_queries = Queries.load(cur_dir + '/data/fold'+str(fold_n)+'_vali')
        test_queries = Queries.load(cur_dir + '/data/fold'+str(fold_n)+'_test')

    logging.info('================================================================================')

    return train_queries, valid_queries, test_queries

def eval_model(model, train_queries, valid_queries, test_queries, model_args=dict()):
    # Print basic info about query datasets.
    logging.info('Train queries: %s' % train_queries)
    logging.info('Valid queries: %s' % valid_queries)
    logging.info('Test queries: %s' % test_queries)

    logging.info('================================================================================')

    # Prepare metric for this set of queries.
    metric = NormalizedDiscountedCumulativeGain(10, queries=[train_queries, valid_queries, test_queries])

    # Initialize LambdaMART model and train it.
    model = model(**model_args)
    model.fit(metric, train_queries, validation=valid_queries)

    logging.info('================================================================================')

    # Print out the performance on the test set.
    evaluation_score = metric.evaluate_queries(test_queries, model.predict(test_queries))
    logging.info('%s on the test queries: %.8f' % ( metric, evaluation_score))

    return evaluation_score


#Clear file
with open('results.txt','w+') as fh:
    pass


#LAMBDAMART
logging.info('Training LambdaMART')
logging.info('================================================================================')

model_args = {
        'n_estimators' : 1000, #Will stop early 
        'max_depth' : 4, 
        'shrinkage' : 0.08,
        'estopping' : 100,
        'n_jobs' : -1
        }
evals = np.zeros(5,dtype = np.float64)
for i in range(5):
    train_queries, valid_queries, test_queries = load_data(i+1)
    evals[i] = eval_model(LambdaMART, train_queries, valid_queries, test_queries, model_args = model_args)

print evals
with open('results.txt','a+') as fh:
    fh.write("LambdaMART\nNDCG\nmean: %f \t std: %f\n\n" % (np.mean(evals),np.std(evals)))

#LAMBARANDOMFOREST

logging.info('Training LambdaRandomForest')
logging.info('================================================================================')

model_args = {
        'n_estimators':rf_trees, 
        'max_depth':4, 
        'n_jobs':-1
        }
evals = np.zeros(5,dtype = np.float64)
for i in range(5):
    train_queries, valid_queries, test_queries = load_data(i+1)
    evals[i] = eval_model(LambdaRandomForest, train_queries, valid_queries, test_queries, model_args = model_args)

print evals
with open('results.txt','a+') as fh:
    fh.write("LambdaRandomForest\nNDCG\nmean: %f \t std: %f\n\n" % (np.mean(evals),np.std(evals)))




#McRank GBRT

logging.info('Training McRank GBRT')
logging.info('================================================================================')


model_args = {
        'estimator' : GradientBoostingClassifier,
        'clf_args' : {
                'n_estimators': gbrt_estimators,
                'verbose' : verbosity
            }
        }
evals = np.zeros(5,dtype = np.float64)
for i in range(5):
    train_queries, valid_queries, test_queries = load_data(i+1)
    evals[i] = eval_model(McRank, train_queries, valid_queries, test_queries, model_args = model_args)

with open('results.txt','a+') as fh:
    fh.write("McRank - GBRT\nNDCG\nmean: %f \t std: %f\n\n" % (np.mean(evals),np.std(evals)))


#McRank RF
logging.info('Training McRank RF')
logging.info('================================================================================')

model_args = {
        'estimator' : RandomForestClassifier,
        'clf_args' : {
                    'n_estimators': rf_trees, 
                    'max_depth':4,
                    'verbose' : verbosity,
                    'n_jobs' : -1
            }
        }
evals = np.zeros(5,dtype = np.float64)
for i in range(5):
    train_queries, valid_queries, test_queries = load_data(i+1)
    evals[i] = eval_model(McRank, train_queries, valid_queries, test_queries, model_args = model_args)

with open('results.txt','a+') as fh:
    fh.write("McRank - RF\nNDCG\nmean: %f \t std: %f\n\n" % (np.mean(evals),np.std(evals)))


#McRank iGBRT

logging.info('Training McRank iGBRT')
logging.info('================================================================================')

model_args = {
        'estimator' : GradientBoostingClassifier,
        'clf_args' : {
                'init': init_multiclass(RandomForestClassifier(n_estimators=rf_trees_igbrt, n_jobs=2, verbose=verbosity)),
                'n_estimators':gbrt_estimators,
                'verbose' : verbosity,
                }
        }
evals = np.zeros(5,dtype = np.float64)
for i in range(5):
    train_queries, valid_queries, test_queries = load_data(i+1)
    evals[i] = eval_model(McRank, train_queries, valid_queries, test_queries, model_args = model_args)

with open('results.txt','a+') as fh:
    fh.write("McRank - iGBRT\nNDCG\nmean: %f \t std: %f\n\n" % (np.mean(evals),np.std(evals)))

#OrdinalMcRank GBRT

logging.info('Training OrdinalMcRank GBRT')
logging.info('================================================================================')


model_args = {
        'estimator' : GradientBoostingClassifier,
        'clf_args' : {
                'n_estimators': gbrt_estimators,
                'verbose' : verbosity
            }
        }
evals = np.zeros(5,dtype = np.float64)
for i in range(5):
    train_queries, valid_queries, test_queries = load_data(i+1)
    evals[i] = eval_model(OrdinalMcRank, train_queries, valid_queries, test_queries, model_args = model_args)

with open('results.txt','a+') as fh:
    fh.write("OrdinalMcRank - GBRT\nNDCG\nmean: %f \t std: %f\n\n" % (np.mean(evals),np.std(evals)))


#OrdinalMcRank RF
logging.info('Training OrdinalMcRank RF')
logging.info('================================================================================')

model_args = {
        'estimator' : RandomForestClassifier,
        'clf_args' : {
                    'n_estimators': rf_trees, 
                    'max_depth':4,
                    'verbose' : verbosity,
                    'n_jobs' : -1

            }
        }
evals = np.zeros(5,dtype = np.float64)
for i in range(5):
    train_queries, valid_queries, test_queries = load_data(i+1)
    evals[i] = eval_model(OrdinalMcRank, train_queries, valid_queries, test_queries, model_args = model_args)

with open('results.txt','a+') as fh:
    fh.write("OrdinalMcRank - RF\nNDCG\nmean: %f \t std: %f\n\n" % (np.mean(evals),np.std(evals)))


#OrdinalMcRank iGBRT
logging.info('Training OrdinalMcRank iGBRT')
logging.info('================================================================================')

model_args = {
        'estimator' : GradientBoostingClassifier,
        'clf_args' : {
                'init': init_singleclass(RandomForestClassifier(n_estimators=rf_trees_igbrt, n_jobs=-1, verbose=verbosity)),
                'n_estimators':gbrt_estimators,
                'verbose' : verbosity,
                },
        }
evals = np.zeros(5,dtype = np.float64)
for i in range(5):
    train_queries, valid_queries, test_queries = load_data(i+1)
    evals[i] = eval_model(OrdinalMcRank, train_queries, valid_queries, test_queries, model_args = model_args)

with open('results.txt','a+') as fh:
    fh.write("OrdinalMcRank - iGBRT\nNDCG\nmean: %f \t std: %f\n\n" % (np.mean(evals),np.std(evals)))



#RegressionMcRank GBRT

logging.info('Training RegressionMcRank GBRT')
logging.info('================================================================================')


model_args = {
        'estimator' : GradientBoostingRegressor,
        'clf_args' : {
                'n_estimators': gbrt_estimators,
                'verbose' : verbosity
            }
        }
evals = np.zeros(5,dtype = np.float64)
for i in range(5):
    train_queries, valid_queries, test_queries = load_data(i+1)
    evals[i] = eval_model(RegressionMcRank, train_queries, valid_queries, test_queries, model_args = model_args)

with open('results.txt','a+') as fh:
    fh.write("RegressionMcRank - GBRT\nNDCG\nmean: %f \t std: %f\n\n" % (np.mean(evals),np.std(evals)))


#RegressionMcRank RF
logging.info('Training RegressionMcRank RF')
logging.info('================================================================================')

model_args = {
        'estimator' : RandomForestRegressor,
        'clf_args' : {
                    'n_estimators': rf_trees, 
                    'max_depth':4,
                    'verbose' : verbosity,
                    'n_jobs' : -1

            }
        }
evals = np.zeros(5,dtype = np.float64)
for i in range(5):
    train_queries, valid_queries, test_queries = load_data(i+1)
    evals[i] = eval_model(RegressionMcRank, train_queries, valid_queries, test_queries, model_args = model_args)

with open('results.txt','a+') as fh:
    fh.write("RegressionMcRank - RF\nNDCG\nmean: %f \t std: %f\n\n" % (np.mean(evals),np.std(evals)))


#RegressionMcRank iGBRT
logging.info('Training RegressionMcRank iGBRT')
logging.info('================================================================================')

model_args = {
        'estimator' : GradientBoostingRegressor,
        'clf_args' : {
                'init': init_singleclass(RandomForestClassifier(n_estimators=rf_trees_igbrt, n_jobs=-1, verbose=verbosity)),
                'n_estimators':gbrt_estimators,
                'verbose' : verbosity
                }
        }
evals = np.zeros(5,dtype = np.float64)
for i in range(5):
    train_queries, valid_queries, test_queries = load_data(i+1)
    evals[i] = eval_model(RegressionMcRank, train_queries, valid_queries, test_queries, model_args = model_args)

with open('results.txt','a+') as fh:
    fh.write("RegressionMcRank - iGBRT\nNDCG\nmean: %f \t std: %f\n\n" % (np.mean(evals),np.std(evals)))

