# -*- coding: utf-8 -*-

import numpy as np
import os
import logging

from rankpy.queries import Queries
from rankpy.models import LambdaMART
from rankpy.metrics import NormalizedDiscountedCumulativeGain

from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier

class init:
    def __init__(self, est):
        self.est = rf

    def predict(self, X):
        return self.est.predict_proba(X)

    def fit(self, X, y):
        self.est.fit(X, y.astype(float))

# Turn on logging.
logging.basicConfig(format='%(asctime)s : %(message)s', level=logging.INFO)

cur_dir = os.path.dirname(os.path.realpath(__file__))

rf_ndcgs = []
gbrt_ndcgs = []
igbrt_ndcgs = []

rf_estimators = 1000
gbrt_estimators = 300
n_jobs = 2
learning_rate = 0.1

for i in xrange(1, 6):
    name =  'Fold' + str(i)

    train_filepath = cur_dir + '/data/MQ2008/' + name + '/train.txt'
    valid_filepath = cur_dir + '/data/MQ2008/' + name + '/vali.txt'
    test_filepath = cur_dir + '/data/MQ2008/' + name + '/test.txt'

    # Load the query datasets.
    train_queries = Queries.load_from_text(train_filepath)
    valid_queries = Queries.load_from_text(valid_filepath)
    test_queries = Queries.load_from_text(test_filepath)

    logging.info('================================================================================')

    # Save them to binary format ...
    train_queries.save(cur_dir+'/data/'+name+'_train')
    valid_queries.save(cur_dir+'/data/'+name+'_vali')
    test_queries.save(cur_dir+'/data/'+name+'_test')

# ... because loading them will be then faster.
    train_queries = Queries.load(cur_dir+'/data/'+name+'_train')
    valid_queries = Queries.load(cur_dir+'/data/'+name+'_vali')
    test_queries = Queries.load(cur_dir+'/data/'+name+'_test')

    logging.info('================================================================================')

    # Print basic info about query datasets.
    logging.info('Train queries: %s' % train_queries)
    logging.info('Valid queries: %s' % valid_queries)
    logging.info('Test queries: %s' % test_queries)

    logging.info('================================================================================')

    # Prepare metric for this set of queries.
    metric = NormalizedDiscountedCumulativeGain(10, queries=[train_queries, test_queries])

    X_train = train_queries.feature_vectors
    y_train = train_queries.relevance_scores.astype(float)

    X_valid = valid_queries.feature_vectors
    y_valid = valid_queries.relevance_scores.astype(float)

    X_test = test_queries.feature_vectors
    y_test = test_queries.relevance_scores.astype(float)

    print ''
    logging.info('+====================RF====================+')

    rf = RandomForestClassifier(n_estimators=rf_estimators, n_jobs=n_jobs, verbose=1)
    rf.fit(X_train, y_train)

    # Print out the performance on the test set.
    print rf
    print ''

    ndcg = metric.evaluate_queries(test_queries, rf.predict(X_test))
    rf_ndcgs.append(ndcg)

    logging.info('%s Random Forest on the test queries: %.8f' % (
    metric, ndcg))

    print ''
    logging.info('+====================GBRT====================+')

    gbrt = GradientBoostingClassifier(n_estimators=gbrt_estimators, verbose=1)
    gbrt.fit(X_train, y_train)

    print gbrt
    print ''

    ndcg = metric.evaluate_queries(test_queries, gbrt.predict(X_test))
    gbrt_ndcgs.append(ndcg)

    # Print out the performance on the test set.
    logging.info('%s GBRT on the test queries: %.8f' % (
    metric, ndcg))

    print ''
    logging.info('+====================IGBRT====================+')

    rf_base = RandomForestClassifier(n_estimators=rf_estimators, n_jobs=n_jobs, verbose=1)
    base_estimator = init(rf_base)
    igbrt = GradientBoostingClassifier(n_estimators=gbrt_estimators, init=base_estimator, verbose=1)
    igbrt.fit(X_train, y_train)

    print igbrt
    print ''

    ndcg = metric.evaluate_queries(test_queries, igbrt.predict(X_test))
    igbrt_ndcgs.append(ndcg)

    # Print out the performance on the test set.
    logging.info('%s IGBRT on the test queries: %.8f' % (
    metric, ndcg))

    logging.info('================================================================================')
    print ''

print 'Mean NDCG RF =', np.mean(rf_ndcgs)
print 'Mean NDCG GBRT =', np.mean(gbrt_ndcgs)
print 'Mean NDCG IGBRT =', np.mean(igbrt_ndcgs)
