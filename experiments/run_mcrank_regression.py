# -*- coding: utf-8 -*-

import numpy as np
import os
import logging

from rankpy.queries import Queries
# from rankpy.models import LambdaMART
from rankpy.models import RegressionMcRank
from rankpy.metrics import NormalizedDiscountedCumulativeGain

# Turn on logging.
logging.basicConfig(format='%(asctime)s : %(message)s', level=logging.INFO)

cur_dir = os.path.dirname(os.path.realpath(__file__))

# Load the query datasets.
train_queries = Queries.load_from_text(cur_dir + '/data/MQ2008/Fold1/train.txt')
valid_queries = Queries.load_from_text(cur_dir + '/data/MQ2008/Fold1/vali.txt')
test_queries = Queries.load_from_text(cur_dir + '/data/MQ2008/Fold1/test.txt')

logging.info('================================================================================')

# Save them to binary format ...
train_queries.save(cur_dir + '/data/fold1_train')
valid_queries.save(cur_dir + '/data/fold1_vali')
test_queries.save(cur_dir + '/data/fold1_test')

# ... because loading them will be then faster.
train_queries = Queries.load(cur_dir + '/data/fold1_train')
valid_queries = Queries.load(cur_dir + '/data/fold1_vali')
test_queries = Queries.load(cur_dir + '/data/fold1_test')

logging.info('================================================================================')

# Print basic info about query datasets.
logging.info('Train queries: %s' % train_queries)
logging.info('Valid queries: %s' % valid_queries)
logging.info('Test queries: %s' % test_queries)

logging.info('================================================================================')

# Prepare metric for this set of queries.
metric = NormalizedDiscountedCumulativeGain(10, queries=[train_queries, valid_queries, test_queries])

# Initialize LambdaMART model and train it.
model = RegressionMcRank(clf_args=dict(max_depth=4, learning_rate=0.08))
model.fit(metric, train_queries, validation=valid_queries)

logging.info('================================================================================')

# Print out the performance on the test set.
logging.info('%s on the test queries: %.8f' % (
    metric, metric.evaluate_queries(test_queries, model.predict(test_queries))))
