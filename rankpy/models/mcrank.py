from sklearn.base import BaseEstimator, clone
from sklearn.ensemble import GradientBoostingClassifier, GradientBoostingRegressor
from sklearn.preprocessing import LabelEncoder

import os
import logging
import sklearn

import numpy as np

from ..utils import parallel_helper
from ..utils import pickle, unpickle
from ..metrics._utils import ranksort_queries

from .lambdamart import compute_lambdas_and_weights
from .lambdamart import _estimate_newton_gradient_steps


DEFAULT_CLF = GradientBoostingClassifier


class _BaseMcRank(BaseEstimator):
    def __init__(self, estimator=DEFAULT_CLF, clf_args=dict()):
        self.estimator = estimator(**clf_args)
        self._label_encoder = LabelEncoder()

    @property
    def classes_(self):
        return self._label_encoder.classes_


    def predict(self, queries):
        """Predict expected target value for queries.

        Parameters
        ----------
        queries : array-like of shape = [n_samples, n_features]
            The input samples.

        Returns
        -------
        p : array of shape = [n_samples]
        """
        n_samples = queries.document_count()
        proba = self.predict_proba(queries.feature_vectors)
        n_classes = len(self.classes_)

        classes = np.repeat(self.classes_, n_samples)
        classes = classes.reshape(n_classes, n_samples).T

        # pred[i] = \sum_m P(y_i = m) * m
        return np.average(classes, axis=1, weights=proba)

    def predict_ranking(self, queries, compact=False, n_jobs=1):
        if self.trained is False:
            raise ValueError('the model has not been trained yet')

        # Predict the ranking scores for the documents.
        predictions = self.predict(queries, n_jobs)

        rankings = np.zeros(queries.document_count(), dtype=np.intc)

        ranksort_queries(queries.query_indptr, predictions, rankings)

        if compact or queries.query_count() == 1:
            return rankings
        else:
            return np.array_split(rankings, queries.query_indptr[1:-1])


class McRank(_BaseMcRank):
    
    #Needs a classifier as estimator.

    def fit(self, metric, queries, validation=None):
        y = self._label_encoder.fit_transform(queries.relevance_scores)
        self.estimator.fit(queries.feature_vectors, y)

        return self

    def predict_proba(self, X):
        """Predict class probabilities for X.

        Parameters
        ----------
        X : array-like of shape = [n_samples, n_features]
            The input samples.

        Returns
        -------
        p : array of shape = [n_samples, n_classes]
            The class probabilities of the input samples. The order of the
            classes corresponds to that in the attribute `classes_`.
        """
        return self.estimator.predict_proba(X)


class OrdinalMcRank(_BaseMcRank):
    
    #Needs a classifier as estimator.

    def _fit(self, X, y, m):
        cond = y <= m
        y_bin = y.copy()
        y_bin[cond] = 0
        y_bin[~cond] = 1
        estimator = clone(self.estimator)

        estimator.fit(X, y_bin)
        return estimator

    def fit(self, metric, queries, validation=None):
        y = self._label_encoder.fit_transform(queries.relevance_scores)
        # FIXME: can be done in parallel.
        self.estimators = []

        for m in xrange(len(self.classes_) - 1):
            est = self._fit(queries.feature_vectors, y, m)
            self.estimators.append(est)

        return self

    def predict_proba(self, X):
        n_samples = X.shape[0]
        n_classes = len(self.classes_)

        # 2d array of shape (n_classes-1) x n_samples containing
        # cumulative probabilities P(y_i <= k)
        P = np.array([e.predict_proba(X)[:, 0] for e in self.estimators])

        # 2d array of shape n_classes x n_samples containing
        # cumulative probabilities P(y_i <= k)
        P = np.vstack((P, np.ones(n_samples)))

        proba = np.zeros((n_samples, n_classes), dtype=np.float64)

        proba[:, 0] = P[0]  # P(y = 0) = P(y <= 0)

        for m in xrange(1, n_classes):
            proba[:, m] = P[m] - P[m - 1]  # P(y = m) = P(y <= m) - P(y <= m - 1)

        return proba


DEFAULT_REGR = GradientBoostingRegressor

class RegressionMcRank(McRank):

    #Needs a regressor as estimator.

    def __init__(self, estimator = DEFAULT_REGR, clf_args = dict()):
        super(self.__class__, self).__init__(estimator, clf_args)

    def predict(self, queries):
        return self.estimator.predict(queries.feature_vectors)

